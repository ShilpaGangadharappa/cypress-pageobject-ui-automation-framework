import LandingPage from '../pages/LandingPage';
import AddToCartPage from '../pages/AddToCartPage';
import CheckOut from '../pages/CheckOut';

// ### 1. Launch the Application
// ### 2. login to the application
// ### 3. Select the Product
// ### 4. Add to Cart
// ### 5. Check out the product from add to the cart popup
// ### 6. Verify the message displayed that the product is added to Cart
// ### 7. Check the Product Count of the Cart
// ### 8. Proceed to checkout
describe('Shopping :checkout product from "Add to Cart" popup', () => {
    before(function () {
        LandingPage.openApp();
    });

	it('sign in  to the application and stay in home page', () => {
		LandingPage.login();
		LandingPage.navigateToHomePage();
	});
	
    it('Check the title, url and links on the Landing Page', () => {
        LandingPage.verifyTitle();
        LandingPage.verifyUrl();
        LandingPage.verifyForPageToLoad();
        LandingPage.contactLink();
        LandingPage.logOutLink();
    });

    it('click on 20% discount listed Item', () => {
        LandingPage.printedChiffonDress();
        AddToCartPage.clickChiffonDress();
    });

    it('Check whether the Add to Cart Button is enabled and click product to add to Cart', () => {
        AddToCartPage.clickAddToCart();
    });

    it('Verify whether the product added to Cart message is displayed', () => {
        AddToCartPage.cartSuccessMessage();
        AddToCartPage.cartAddedMessage();
        
    });

    it('Verify the product count from the Cart', () => {
        AddToCartPage.cartCount();
        CheckOut.proceedTocheckOutOnCartPopup();
    });
})

// ### 1. Launch the Application
// ### 2. Select the Product
// ### 3. Add to Cart
// ### 4. Verify the message displayed that the product is added to Cart
// ### 5. login to the application
// ### 6. Check the Product Count of the Cart
// ### 7. Check out the product from cart


describe('Shopping :sign-in to the application during checkout of product', () => {
    before(function () {
        LandingPage.openApp();
    });

    it('click on 20% discount listed Item', () => {
        LandingPage.printedChiffonDress();
        AddToCartPage.clickChiffonDress();
    });

    it('Check whether the Add to Cart Button is enabled and click product to add to Cart', () => {
        AddToCartPage.clickAddToCart();
    });

    it('Verify whether the product added to Cart message is displayed', () => {
        AddToCartPage.cartSuccessMessage();
        AddToCartPage.cartAddedMessage();
        
    });

    it('Verify the product count from the Cart', () => {
        AddToCartPage.closeProceedToCheckoutPopup();
        AddToCartPage.cartCount();
    });

    it('verfify checkout from cart', () => {
        CheckOut.clickOnCart();
        CheckOut.cleckOut();
    });   

    it('sign in  to the application', () => {
        LandingPage.login();
    });
})

// ### 1. Launch the Application
// ### 2. login to the application
// ### 3. Select the Product
// ### 4. Add to Cart
// ### 5. Verify the message displayed that the product is added to Cart
// ### 6. logout from application
// ### 7. login from application
// ### 8. Check the Product Count of the Cart
// ### 9. Check out the product from cart

describe('Shopping :sign-in to the application and product to cart, sign out and sine-in again to checkout', () => {
    
    before(function () {
        LandingPage.openApp();
    });

    it('sign in  to the application and stay in home page', () => {
        LandingPage.login();
        LandingPage.navigateToHomePage();
    });

    it('click on 20% discount listed Item', () => {
        LandingPage.printedChiffonDress();
        AddToCartPage.clickChiffonDress();
    });
    
    it('Check whether the Add to Cart Button is enabled and click product to add to Cart', () => {
        AddToCartPage.clickAddToCart();
    });

    it('Verify whether the product added to Cart message is displayed', () => {
        AddToCartPage.cartSuccessMessage();
        AddToCartPage.cartAddedMessage();
        
    });

    it('Verify the product count from the Cart', () => {
        AddToCartPage.closeProceedToCheckoutPopup();
        AddToCartPage.cartCount();
    });

    it('sign in  to the application and stay in home page', () => {
        LandingPage.login();
        LandingPage.navigateToHomePage();
    });

    it('sign out to the application', () => {
        LandingPage.logout();
    });

    it('verfify checkout from cart', () => {
        CheckOut.clickOnCart();
        CheckOut.cleckOut();
    });   

    it('proceed from summery', () => {
        CheckOut.proceedToClickOutInSummary();
    });

})


// ### 1. Launch the Application
// ### 2. login to the application
// ### 3. Select the Product
// ### 4. Add to Cart
// ### 5. Verify the message displayed that the product is added to Cart
// ### 6. Check the Product Count of the Cart
// ### 6. Check out the product from cart

describe('Shopping :Sign in to the application add product to the cart, checkout the products from the cart and order.', () => {
    
    before(function () {
        LandingPage.openApp();
    });

    it('sign in  to the application and stay in home page', () => {
        LandingPage.login();
        LandingPage.navigateToHomePage();
    });

    it('click on 20% discount listed Item', () => {
        LandingPage.printedChiffonDress();
        AddToCartPage.clickChiffonDress();
    });
    
    it('Check whether the Add to Cart Button is enabled and click product to add to Cart', () => {
        AddToCartPage.clickAddToCart();
    });

    it('Verify whether the product added to Cart message is displayed', () => {
        AddToCartPage.cartSuccessMessage();
        AddToCartPage.cartAddedMessage();
        
    });

    it('Verify the product count from the Cart', () => {
        AddToCartPage.closeProceedToCheckoutPopup();
        AddToCartPage.cartCount();
    });

    it('verfify checkout from cart', () => {
        CheckOut.clickOnCart();
        CheckOut.cleckOut();
    });   
})


