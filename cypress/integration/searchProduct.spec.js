import LandingPage from '../pages/LandingPage';
import Search from '../pages/SearchPage';


// ### 1. Launch the Application
// ### 2. Search for the product
// ### 3. Verify the search results
// ### 4. Add to cart
// ### 5. Check Out

describe('Search Product with valid product name', () => {
    before(function () {
        LandingPage.openApp();
    });

	it('search product and verify serach result', () => {
		Search.enterValueForSearchBox();
		Search.verifySearchText(Cypress.env('searchTest'));
	});

    it('search item from search result', () => {
        Search.printedSummerDress();
        Search.clickChiffonDress();
	});

    it('Check whether the Add to Cart Button is enabled and click product to add to Cart', () => {
        Search.clickAddToCart();
    });

    it('Verify whether the product added to Cart message is displayed', () => {
        Search.cartSuccessMessage();
        Search.cartAddedMessage(); 
    });

    it('Verify the product count from the Cart', () => {
        Search.closeProceedToCheckoutPopup();
        Search.cartCount();
    });
    
})

// ### 1. Launch the Application
// ### 2. Search for the invalid product
// ### 3. Verify "No result found" message

describe('Search Product with invalid product name', () => {
    before(function () {
        LandingPage.openApp();
    });

	it('Search product with invalid value', () => {
		Search.enterInvalidValueForSearchBox();
	});

    it('Verify no result found message', () => {
		Search.noResultFound();
	});
})

// ### 1. Launch the Application
// ### 2. Search for the product with partial name
// ### 3. Verify the search results

describe('Search Product with partial product name', () => {
    before(function () {
        LandingPage.openApp();
    });

	it('Search product and verify serach result with partial text', () => {
		Search.enterValueForSearchBoxPartialText();
		Search.verifySearchText('Printed');
	});
})
