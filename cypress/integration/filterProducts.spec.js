import LandingPage from '.././pages/LandingPage';
import ProductPage from '.././pages/ProductPage';
import FilterPage from '.././pages/FilterPage';

// ### 1. Launch the Application
// ### 2. Filter the products
// ### 3. Click on the 5th Product with 5% discount from the list
// ### 4. Toggle between the Colours for this selected product
// ### 5. Verify the image is updated when selecting different colours
// ### 6. Click the Logo at the top and Navigate to Landing Page

describe('Filter the elements and choose the color of the product', () => {
	before(function () {
		LandingPage.openApp();
	});

	it('Check the title, url and links on the Landing Page', () => {
		LandingPage.verifyTitle();
		LandingPage.verifyUrl();
		LandingPage.verifyForPageToLoad();
		LandingPage.contactLink();
		LandingPage.loginLink();
	});

    it('Filter Dress-> Summer Dress', () => {
        FilterPage.filterWomenCategory();
		FilterPage.filterDress();
		FilterPage.filterSummerDress();
	});

	it('Click on the 5th Product with 5% discount on the Landing Page', () => {
		ProductPage.printedSummerDress();
		ProductPage.verifyTitle();
	});

	it('Check when selecting black colour the image is updated', () => {
		ProductPage.blackColour();
		ProductPage.colourSelected();
		ProductPage.blackColourImageUpdated();
	});

	it('Check when selecting orange colour the image is updated', () => {
		ProductPage.orangeColour();
		ProductPage.colourSelected();
		ProductPage.orangeColourImageUpdated();
	});

	it('Check when selecting blue colour the image is updated', () => {
		ProductPage.blueColour();
		ProductPage.colourSelected();
		ProductPage.blueColourImageUpdated();
	});

	it('Check when selecting yellow colour the image is updated', () => {
		ProductPage.yellowColour();
		ProductPage.colourSelected();
		ProductPage.yellowColourImageUpdated();
	});

	it('Navigate to Landing Page', () => {
		ProductPage.navigateToHomePage();
	});
});

// ### 1. Launch the Application
// ### 2. Filter the products
// ### 3. Click on checkbox
// ### 4. add element to cart


describe('Filter the elements and click on checkbox', () => {
	before(function () {
		LandingPage.openApp();
	});

    it('Filter Dress-> Summer Dress', () => {
        FilterPage.filterWomenCategory();
		FilterPage.filterDress();
		FilterPage.filterSummerDress();
		FilterPage.cleckBox();  
	});

    it('Click on check box', () => {
		FilterPage.cleckBox();  
	});

    it('Click on the 5th Product with 5% discount on the Landing Page', () => {
		ProductPage.printedSummerDress();
		ProductPage.verifyTitle();
	});
});
