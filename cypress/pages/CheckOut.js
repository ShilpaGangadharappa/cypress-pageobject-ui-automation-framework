import BasePage from '../pages/BasePage';
import {
	checkout,
	proceedTocheckOutAddress,
	agreeTerms,
	proceedTocheckOutPayment,
	pageheader,
	cart,
	proceedTocheckOutSummary,
	proceedTocheckOutOnCartPopup,
} from '../selectors/locators';

export default class CheckOut extends BasePage {
	
	static clickOnCart() {
		cy.wait(1000);
		return cy.get(cart).click();
	}

    static cleckOut() {
		return cy.get(checkout).click({ force: true });
	}

    static proceedToClickOut() {
		return cy.get('a').contains('Proceed to checkout').click();
	}

    static proceedToClickOutInAddress() {
		return cy.get(proceedTocheckOutAddress).click();
	}

	static proceedToClickOutInSummary() {
		return cy.get(proceedTocheckOutSummary).click();
	}

	static proceedTocheckOutOnCartPopup() {
		return cy.get(proceedTocheckOutOnCartPopup).click();
	}

    static agreeTerms() {
		return cy.get(agreeTerms).click();
	}
	
    static makePayment() {
        cy.get(bankwire).should('be.visible').click()
        return cy.get(proceedTocheckOutPayment).should('be.visible').click()
	}
    static orderconformation() {
        return cy.get(pageheader).contains("Order confirmation")
	}


}
