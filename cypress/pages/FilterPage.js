import {
	filterWomen,
	filterDress,
	filterSummerDress,
	CasualCheckBox,
} from '../selectors/locators';
import ProductPage from './ProductPage';

export default class Filter extends ProductPage {
	
	static filterWomenCategory() {
		return cy.get(filterWomen).click();
	}

	static cleckBox() {
		return cy.get(CasualCheckBox).click();
	}

	static filterDress() {
		return cy.get(filterDress).click();
	}

	static filterSummerDress() {
		return cy.get(filterSummerDress).click();
	}
}
