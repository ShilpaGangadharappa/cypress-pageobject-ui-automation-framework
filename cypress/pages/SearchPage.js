import AddtoCartPage from './AddToCartPage';
import {
	searchTestBox,
	serachFilterMessage,
	selectFromSearchDropdown,
	searchButton,
	alert,
} from '../selectors/locators';

export default class Search extends AddtoCartPage {
	
	static enterValueForSearchBox() {
	   cy.get(searchTestBox).should('be.visible').type(Cypress.env('searchTest'));
		return cy.get(searchButton).click();
	}

	static enterValueForSearchBoxPartialText() {
		cy.get(searchTestBox).should('be.visible').type('Printed');
		 return cy.get(searchButton).click();
	 }

	static enterInvalidValueForSearchBox() {
		cy.get(searchTestBox).should('be.visible').type('invalid');
		 return cy.get(searchButton).click();
	 }

	static selectFromSerachDropdown() {
		cy.wait(10000);
		return cy.get(selectFromSearchDropdown).click();
	}

	static verifySearchText(searchvalue) {
		return cy.get(serachFilterMessage).should('be.visible').contains(searchvalue);
	}


	static printedSummerDress() {
		return cy.get('a').contains('Printed Summer Dress').should('be.visible');
	}

	static clickChiffonDress() {
		return cy.get('a').contains('Printed Summer Dress').click();
	}

	static noResultFound() {
		return cy
			.get(alert)
			.contains('No results were ').should('be.visible');;
			
	}
}
