import { homePageImage, landingPageTitle, usernam, passowrd, loginbutton, logout} from '../selectors/locators';

export default class BasePage {
	static openApp() {
		return cy.openHomePage();
	}

	static pause(timeout) {
		return cy.wait(timeout);
	}

	static verifyTitle() {
		return cy.title().should('eq', landingPageTitle);
	}

	static verifyUrl() {
		return cy.url().should('include', '/index.php');
	}

	static verifyForPageToLoad() {
		return cy.get(homePageImage).should('be.visible');
	}

	static contactLink() {
		return cy.get('a').contains('Contact us').should('be.visible');
	}

	static loginLink() {
		return cy.get('a').contains('Sign in').should('be.visible');
	}
	static logOutLink() {
		return cy.get('a').contains('Sign out').should('be.visible');
	}

	static login() {
		cy.get('a').contains('Sign in').should('be.visible').click()
		cy.get(usernam).should('be.visible').type(Cypress.env('username'))
        cy.get(passowrd).should('be.visible').type(Cypress.env('password'))
        return cy.get(loginbutton).should('be.visible').contains('Sign in').click()
	}

	static logout() {
        return cy.get(logout).click()
	}

	static navigateToHomePage() {
		cy.get(homePageImage).click();
		return cy.title().should('eq', landingPageTitle);
	}

}
