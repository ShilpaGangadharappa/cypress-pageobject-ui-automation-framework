module.exports = {
	//Locators
   // login-logout
    usernam: '#email',
	passowrd: '#passwd',
	loginbutton: '#SubmitLogin',
	logout:'.logout',

	//Checkout 
	cart:'.shopping_cart > a > b',
	checkout: '#button_order_cart > span',
	proceedTocheckOutAddress: '.cart_navigation > .button > span',
	agreeTerms:'#cgv',
	proceedTocheckOutPayment:'#cart_navigation > .button > span',
	bankwire:'.bankwire',
	pageheader:'.page-heading',
	proceedTocheckOutSummary:'.cart_navigation > a > span',
	proceedTocheckOutOnCartPopup:'.button-container > a > span',

	//Search
	searchTestBox:'#search_query_top',
	serachFilterMessage:"#center_column > .page-heading > .lighter",
	selectFromSearchDropdown:'#search > .ac_results  > ul > li',
	searchButton:'#searchbox > button',
	alert:'#center_column .alert',
	
	//Filters
	filterWomen: '#block_top_menu > ul  > li:nth-child(1) > a',
	filterDress: '#categories_block_left .block_content  ul:nth-child(1) > li:nth-child(2) > a:nth-child(2)',
	filterSummerDress:'#categories_block_left  > .block_content > .tree > li:nth-child(3) > a',
	CasualCheckBox:'#layered_id_feature_11',
	
	//Product
	homePageImage: 'div#header_logo',
	landingPageTitle: 'My Store',
	printedDressTwo:
		'#homefeatured > .last-in-line > .product-container > .right-block > h5 > .product-name',
	printedSummerDressTwo:
		'#homefeatured > .last-line.last-item-of-tablet-line > .product-container > .right-block > h5 > .product-name',
	addToCartButton: '#add_to_cart > .exclusive',
	successMessage:
		'#layer_cart > div.clearfix > div.layer_cart_product.col-xs-12.col-md-6 > h2',
	cartAddedMessage:
		'#layer_cart > div.clearfix > div.layer_cart_cart.col-xs-12.col-md-6',
	closeBtn: 'span.cross',
	itemCount: 'div > a > span.ajax_cart_quantity',
	priceCount: 'div > a > span.ajax_cart_total.unvisible',
	productCount: '1',
	colourBlack: '#color_11',
	colourOrange: '#color_13',
	colourBlue: '#color_14',
	colourYellow: '#color_16',
	colourSelected: '#color_to_pick_list > li.selected',
	imagePicture: '#bigpic',
	quantityWanted: '#quantity_wanted',
	numberOfItems: '5',
	checkout:
		'#layer_cart > div.clearfix > div.layer_cart_cart.col-xs-12.col-md-6 > div.button-container > a > span',
	productPrice: '#total_product',
	totalProductPrice: '$144.90',
};
