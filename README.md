# cypress-pageobject-ui-automation-framework
Cypress UI automation Framework
 
 Framework is built on Cypress using Page Object Model(POM) design pattern and Mocha is used to genareate the reports.

 application used to Automate: http://automationpractice.com/index.php
 
**FrameWork structure**

![](img/FrameDetails.PNG)

![](img/FrameworkStructure.PNG)

**Acceptance criteria**

1.	Verify user should be able to add all products to cart
2.	Verify user should be able to check out and order all the products in the cart
3.	Verify user should be able to search the products.
4.	Verify User should be able to Filter the products.

**Scenarios covered in automation :**

**a. Scenarios on add product to the cart and checkout (Shopping)**

1.	Add product to cart and sign in to the application during the checkout
2.	Sign to the application, Add product to the cart, sign out and sign in again to order the product 
3.	Sign in to the application add product to the cart, checkout the products from the cart and order.
4.	Sign in to the application , add product to the cart and proceed to checkout directly from the ‘Add to cart’ popup

**b. Scenarios on Search**
1.	Search with valid input
2.	Search with invalid input
3.	Search with partial match text

**c. Scenarios on Filter**
1.	Filter by Category, Add product to cart by selecting select the color and quantity 
2.	Filter by Category, filter by size, color sand style


**Bugs in the Application**

1. clicking on color, size etc check boxs page keeps loading
3. Sign to the application, Add product to the cart, sign out and sign in again to order the product
  Actual :- cart will be empty
  Expected :- items should remain in the cart even after re-login

![](img/Loading.PNG)


**How to execute test cases ?**

1. clone repo
2. npm install
3. To run cases from command promt: npx cypress run
4. To run cases in crome : npm test:chrome
5. To run cases from Cypress UI : npx cypress open and click on the script you want to execute
     
**Mocha Report in Report folder**

![](img/MachaReport.PNG)


